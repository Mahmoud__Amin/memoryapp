import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:MemoryApp/modules/login/cubit/cubit.dart';
import 'package:MemoryApp/modules/on_boarding/on_boarding_screen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:MemoryApp/shared/blocobserver.dart';
import 'package:MemoryApp/shared/components/constans.dart';
import 'package:MemoryApp/shared/cubit/cubit.dart';
import 'package:MemoryApp/shared/network/local/cache_helper.dart';
import 'package:MemoryApp/shared/network/remote/dio_helper.dart';
import 'package:MemoryApp/shared/notification_service/notification_service.dart';
import 'package:MemoryApp/shared/styles/styles.dart';
import 'package:MemoryApp/flutter_text_to_voice_helper.dart';
import 'layouts/layout_screen.dart';
import 'modules/login/login_screen.dart';
import 'modules/register/cubit/cubit.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await CacheHelper.init();
  NotificationService().initNotification();
  FlutterTextToVoiceHelper.initTts();
  bool? OnBoarding=CacheHelper.getDate(key: 'OnBoarding');
  token=CacheHelper.getDate(key: 'token');
  print("Token from the main $token");
  Widget widget=OnBoardingScreen();
  if(token!=null){
    widget=LayoutScreen();
  }
  else if(OnBoarding!=null){
    widget=LoginScreen();
  }
  DioHelper.init();
  BlocOverrides.runZoned(
        ()  => {
      runApp(MyApp(widget)),
    },
    blocObserver: MyBlocObserver(),
  );
}
class MyApp extends StatelessWidget {
  final Widget widget;
  MyApp(this.widget);
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create:(context)=>LanguageAppCubit()..createDataBase()..getUserData()),
        BlocProvider(create:(context)=>LoginCubit()),
        BlocProvider(create:(context)=>RegisterCubit()),
      ],
      child: ScreenUtilInit(

        builder: (BuildContext context, Widget? child) {
           return MaterialApp(
               debugShowCheckedModeBanner: false,
               theme:lightTheme,
               darkTheme: darkTheme,
               themeMode: ThemeMode.light,
               home: widget
           );
        },
      ),
    );
  }
}


