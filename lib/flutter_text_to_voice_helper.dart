import 'dart:async';
import 'dart:io' show Platform;
import 'package:flutter/foundation.dart' show kIsWeb;

import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';


class FlutterTextToVoiceHelper{

  static FlutterTts? flutterTts;

  static Future<void> initTts() async {
    flutterTts = FlutterTts();
    _setAwaitOptions();
    _getDefaultEngine();
    await flutterTts?.setVolume(0.5);
    await flutterTts?.setSpeechRate(0.5);
    await flutterTts?.setPitch(0.5);
    await flutterTts?.setLanguage('en-us');
  }
  static Future _getDefaultEngine() async {
    var engine = await flutterTts?.getDefaultEngine;
    if (engine != null) {
      print(engine);
    }
  }

  /* double volume = 0.5;
  double pitch = 1.0;
  double rate = 0.5;*/
  static Future<void> _speak(String? text) async {

    if (text != null) {
      if (text.isNotEmpty) {
        await flutterTts?.speak(text);
      }
    }
  }
   static Future _setAwaitOptions() async {
    await flutterTts?.awaitSpeakCompletion(true);
  }


}



