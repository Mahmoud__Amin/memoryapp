import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:MemoryApp/shared/components/components.dart';
import 'package:MemoryApp/shared/cubit/cubit.dart';
import 'package:MemoryApp/shared/cubit/states.dart';

import '../../components/components.dart';
import '../../shared/styles/color.dart';

class ReviewSessionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LanguageAppCubit, LanguageAppStates>(
        listener: (BuildContext context, Object? state) {},
        builder: (BuildContext context, state) {
          var cubit = LanguageAppCubit.get(context);
          return ConditionalBuilder(
              fallback: (BuildContext context) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Center(
                    child: Text(
                      "To start a quiz you should have at least one non-empty group",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                );
              },
              condition: cubit.groups.isNotEmpty,
              builder: (BuildContext context) {
                return buildGridView(context, true);
              });
        });
  }

  void explainTheQuizIdea(context) {
    AlertDialog alertDialog = AlertDialog(
      title: Center(
          child: Text(
              "after pressing on ok you will be in the quiz screen , we make this screen to test yourself and show your memorize percentage for each group",
              style: Theme.of(context).textTheme.bodyText1)),
      backgroundColor: defaultColor1,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: defaultColor, width: 3),
        borderRadius: const BorderRadius.all(Radius.circular(25.0)),
      ),
      content: SizedBox(
        width: 75,
        height: 45,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Row(
              children: [
                const Spacer(),
                defaultButton(
                  background: defaultColor,
                  text: "Ok",
                  width: 100,
                  radius: 25,
                  function: () {
                    Navigator.pop(context);
                  },
                ),
                const Spacer(),
              ],
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (context) => alertDialog);
  }
}
