import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:MemoryApp/shared/components/components.dart';
import 'package:MemoryApp/shared/cubit/cubit.dart';

import '../../components/components.dart';
import '../../flutter_text_to_voice_helper.dart';
import '../../models/word_model.dart';
import '../../shared/cubit/states.dart';
import '../../shared/styles/color.dart';

class FavoriteScreen extends StatelessWidget {
  List<WordModel> favorites=[];

  @override
  Widget build(BuildContext context) {
    var cubit=LanguageAppCubit.get(context);
    return BlocConsumer<LanguageAppCubit,LanguageAppStates>(
      listener: (BuildContext context, state) {  },
      builder: (BuildContext context, Object? state) {
        favorites=[];
        for(int i=0;i<cubit.words.length;++i){
          if(cubit.words[i]?.inFavorite==1){
            favorites.add(cubit.words[i]!);
          }
        }
        return ConditionalBuilder(
          fallback: (BuildContext context) {
              return  Padding(
                padding: const EdgeInsets.symmetric(horizontal: 3),
                child: Center(child: Text("No favorite words yet , start add some !",style:Theme.of(context).textTheme.bodyText1,),),
              );
          },
          condition:(favorites.length!=0),
          builder: (BuildContext context) {
            return SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  children: [
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) =>
                          buildWordsItem(context, index),
                      //separatorBuilder: (BuildContext context, int index)=>divider(),
                      itemCount: favorites.length,
                    ),
                  ],
                ));
          },

        );
      },
    );
  }
  Widget buildWordsItem(context, int index) {
    TextEditingController editWordController =
    TextEditingController(text: favorites[index].word);
    TextEditingController editWordMeaningController =
    TextEditingController(text: favorites[index].wordMeaning);
    return BlocConsumer<LanguageAppCubit, LanguageAppStates>(
      listener: (BuildContext context, Object? state) {},
      builder: (BuildContext context, state) {
        LanguageAppCubit cubit = LanguageAppCubit.get(context);
        var editKey=GlobalKey<FormState>();
        return Padding(
          padding: const EdgeInsets.all(15),
          child: Card(
            shadowColor: defaultColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25),
                side: BorderSide(color: defaultColor, width: 2)),
            semanticContainer: false,
            color: defaultColor2,
            elevation: 20,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: SizedBox(
                  width: double.infinity,
                  child: Form(
                    key: editKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            const SizedBox(
                              width: 18,
                            ),
                            Expanded(
                              child: SizedBox(
                                child: TextFormField(
                                  enableInteractiveSelection: true,
                                  focusNode: FocusNode(),
                                  controller: editWordController,
                                  cursorColor: defaultColor,
                                  readOnly: (favorites[index].inEdit == 0),
                                  validator: (String? value){
                                    if(value!.isEmpty||value.trim().isEmpty){
                                      return "Word must not be Empty";
                                    }
                                    return null;
                                  },
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(fontSize: 26),
                                  decoration:  const InputDecoration(
                                    border: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    focusedErrorBorder:InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                  ),
                                ),
                              ),
                            ),
                            if (favorites[index].inEdit == 1)
                              IconButton(
                                onPressed: () {
                                  if(editKey.currentState!.validate()){
                                    ensureEditWordDialog(
                                        context,
                                        index,
                                        editWordController,
                                        editWordMeaningController);
                                  }

                                },
                                icon: Icon(Icons.save),
                              ),
                          ],
                        ),
                        const SizedBox(
                          height: 3,
                        ),
                        divider(),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                IconButton(
                                  onPressed: () {
                                    FlutterTextToVoiceHelper.flutterTts?.speak(
                                        "${favorites[index].word}  ${favorites[index].wordMeaning}");
                                  },
                                  icon: const ImageIcon(
                                    AssetImage("assets/images/speaker.png"),
                                    size: 24,
                                  ),
                                ),
                                IconButton(
                                    onPressed: () {
                                      cubit.changeFavoriteState(
                                          favorites[index].wordId,
                                          favorites[index].inFavorite);
                                    },
                                    icon: Icon(
                                        (favorites[index].inFavorite ==
                                            1)
                                            ? Icons.favorite
                                            : Icons.favorite_border_outlined)),
                                IconButton(
                                    onPressed: () {
                                      if (favorites[index].inEdit == 0)
                                        explainingHowToEditDialog(context, index);
                                      //cubit.changeEditState(cubit.words[index]?.wordId,cubit.words[index]?.inEdit);
                                    },
                                    icon: Icon(Icons.edit)),
                                IconButton(
                                    onPressed: () {
                                      deleteWordDialog(context, index);
                                      //cubit.deleteFromWordsTable(id: wordsFromThisGroup[index]['word_id'],);
                                    },
                                    icon: Icon(
                                      Icons.delete,
                                    ))
                              ],
                            ),
                            divider1(),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 0, bottom: 15, left: 15, right: 15),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      child: TextFormField(

                                        focusNode: FocusNode(),
                                        controller: editWordMeaningController,
                                        cursorColor: defaultColor,
                                        validator: (String? value){
                                          if(value!.isEmpty||value.trim().isEmpty){
                                            return "Word meaning must not be empty";
                                          }
                                          return null;
                                        },
                                        decoration:  const InputDecoration(
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          focusedErrorBorder:InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                        ),
                                        readOnly:
                                        (favorites[index].inEdit ==
                                            0),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16,
                                          color: defaultColor,
                                        ),
                                        minLines: 1,
                                        maxLines: 500,
                                        toolbarOptions: ToolbarOptions(
                                          paste: true,
                                          cut: true,
                                          copy: true,
                                          selectAll: true,
                                        ),
                                      ),
                                      width: double.infinity,
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )),
            ),
          ),
        );
      },
    );
  }
  void ensureEditWordDialog(context, index,editWordController,editWordMeaningController) {
    var cubit = LanguageAppCubit.get(context);
    AlertDialog alertDialog = AlertDialog(
      title: Center(
          child: Text("Are you sure that you want to save this change ?",
              style: Theme.of(context).textTheme.bodyText1)),
      backgroundColor: defaultColor1,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: defaultColor, width: 3),
        borderRadius: const BorderRadius.all(Radius.circular(25.0)),
      ),
      content: SizedBox(
        width: 75,
        height: 45,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Row(
              children: [
                defaultButton(
                  background: defaultColor,
                  text: "No",
                  width: 100,
                  radius: 25,
                  function: () {
                    Navigator.pop(context);
                    cubit.changeEditState(favorites[index].wordId,favorites[index].inEdit);
                  },
                ),
                const Spacer(),
                defaultButton(
                  background: defaultColor,
                  text: "Yes",
                  width: 100,
                  radius: 25,
                  function: () {
                    //cubit.changeEditState(wordsFromThisGroup[index].wordId,wordsFromThisGroup[index].inEdit);
                    cubit.updateWordTableData(
                        id: favorites[index].wordId,
                        word: editWordController.text,
                        wordMeaning: editWordMeaningController.text,
                        inEdit: 0

                    );
                    Navigator.pop(context);

                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (context) => alertDialog);
  }
  void explainingHowToEditDialog(context,index){
    var cubit = LanguageAppCubit.get(context);
    AlertDialog alertDialog = AlertDialog(
      title: Center(
          child: Text("Now you can edit the words , just press on save icon when you finish to save your adjustment",
              style: Theme.of(context).textTheme.bodyText1)),
      backgroundColor: defaultColor1,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: defaultColor, width: 3),
        borderRadius: const BorderRadius.all(Radius.circular(25.0)),
      ),
      content: SizedBox(
        width: 75,
        height: 45,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Row(
              children: [
                const Spacer(),
                defaultButton(
                  background: defaultColor,
                  text: "Ok",
                  width: 100,
                  radius: 25,
                  function: () {
                    Navigator.pop(context);
                    cubit.changeEditState(favorites[index].wordId,favorites[index].inEdit);
                  },
                ),
                const Spacer(),
              ],
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (context) => alertDialog);
  }
  void deleteWordDialog(context, index) {
    var cubit = LanguageAppCubit.get(context);
    AlertDialog alertDialog = AlertDialog(
      title: Center(
          child: Text("Are you sure you want to delete this word ?",
              style: Theme.of(context).textTheme.bodyText1)),
      backgroundColor: defaultColor1,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: defaultColor, width: 3),
        borderRadius: const BorderRadius.all(Radius.circular(25.0)),
      ),
      content: SizedBox(
        width: 75,
        height: 45,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Row(
              children: [
                defaultButton(
                  background: defaultColor,
                  text: "No",
                  width: 100,
                  radius: 25,
                  function: () {
                    Navigator.pop(context);
                  },
                ),
                const Spacer(),
                defaultButton(
                  background: defaultColor,
                  text: "Yes",
                  width: 100,
                  radius: 25,
                  function: () {
                    cubit.deleteFromWordsTable(
                      id: favorites[index].wordId,
                    );
                    Navigator.pop(context);
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (context) => alertDialog);
  }
}
