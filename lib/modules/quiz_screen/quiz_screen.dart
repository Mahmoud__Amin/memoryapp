import 'dart:math';

import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:MemoryApp/layouts/layout_screen.dart';
import 'package:MemoryApp/modules/quiz_screen/paper_field.dart';

import '../../components/components.dart';
import '../../models/group_model.dart';
import '../../models/word_model.dart';
import '../../shared/components/components.dart';
import '../../shared/components/constans.dart';
import '../../shared/cubit/cubit.dart';
import '../../shared/cubit/states.dart';
import '../../shared/styles/color.dart';
import 'package:assets_audio_player/assets_audio_player.dart';

int index = 0;
Random r = Random();

class QuizScreen extends StatelessWidget {
  final GroupModel group;

  QuizScreen({required this.group, index}){

  }
  List<WordModel> wordsFromThisGroup = [], helperListToDeleteTheElement = [];
  int questionNumber = 1;
  double numberOfCorrectAnswer = 0.0;

  @override
  Widget build(BuildContext context) {
    var cubit = LanguageAppCubit.get(context);

    for (int i = 0; i < cubit.words.length; ++i) {
      if (cubit.words[i]?.groupId == group.groupId) {
        wordsFromThisGroup.add(cubit.words[i]!);
        helperListToDeleteTheElement.add(cubit.words[i]!);
      }
    }
    int tempIndex=0;
    if(wordsFromThisGroup.isNotEmpty) {
      tempIndex = r.nextInt(helperListToDeleteTheElement.length);
      cubit.quizPageIndex = tempIndex;
    }
      return BlocConsumer<LanguageAppCubit, LanguageAppStates>(
        listener: (BuildContext context, state) {},
        builder: (BuildContext context, Object? state){
          return Scaffold(
              appBar: AppBar(
                toolbarHeight: 56,
                title: const Text("The Quiz"),
              ),
              body: ConditionalBuilder(
                builder: (BuildContext context) {
                  return SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Image(
                          image: AssetImage((selectImage(group.groupCategory!))),
                          fit: BoxFit.cover,
                          width: double.infinity,
                          height: 200,
                        ),
                        Container(
                          margin: const EdgeInsets.all(15),
                          padding: const EdgeInsets.all(15),
                          //height: 100,
                          width: double.infinity,
                          child: Center(
                              child: Text(
                                "What the meaning of ${helperListToDeleteTheElement[cubit.quizPageIndex].word} ?",
                                style: Theme.of(context).textTheme.bodyText1,
                              )),
                          decoration: BoxDecoration(
                              color: defaultColor2,
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(
                                color: defaultColor,
                                width: 3,
                              )),
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 35),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Answer here please:",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    ?.copyWith(fontSize: 16),
                              ),
                              const SizedBox(
                                height: 2,
                              ),
                              Container(
                                width: double.infinity,
                                child: PaperField(initialText: "Hey Am initial Text"),
                                color: defaultColor2,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Question number is $questionNumber/${wordsFromThisGroup.length}",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              ?.copyWith(fontSize: 15),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Row(
                          children: [
                            const SizedBox(
                              width: 20,
                            ),
                            defaultButton(
                              function: () {
                                resetQuizScreen(context);
                              },
                              background: defaultColor,
                              text: "Cancel",
                              width: 120,
                              radius: 25,
                            ),
                            const Spacer(),
                            defaultButton(
                              function: () {
                                if (questionNumber != wordsFromThisGroup.length) {
                                  if (stringValidator(helperListToDeleteTheElement[tempIndex].wordMeaning!) ==stringValidator(paperEditingController!.text)) {playCorrectAnswerAudio();
                                  numberOfCorrectAnswer += 1.0;
                                  } else {
                                    playWrongAnswerAudio();
                                  }
                                  helperListToDeleteTheElement.removeAt(tempIndex);
                                  tempIndex = r.nextInt(helperListToDeleteTheElement.length);
                                  cubit.goToNextQuestion(tempIndex);
                                  questionNumber++;
                                  paperEditingController?.text = "";
                                }
                                else {
                                  if (stringValidator(helperListToDeleteTheElement[tempIndex].wordMeaning!) == stringValidator(paperEditingController!.text)){
                                    numberOfCorrectAnswer += 1.0;
                                    playCorrectAnswerAudio();
                                  }
                                  else{
                                    playWrongAnswerAudio();
                                  }
                                  showFinishQuizDialog(context);
                                }
                              },
                              background: defaultColor,
                              text: "Next",
                              width: 120,
                              radius: 25,
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  );
                },
                fallback: (BuildContext context) {
                  return  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Center(child: Text("You can not start a quiz with an empty group !  Add some words to the group and retry.",style:Theme.of(context).textTheme.bodyText1),),
                  );
                },
                condition: wordsFromThisGroup.isNotEmpty,
              ));
        },
      );
    }

  void resetQuizScreen(context) {
    LanguageAppCubit.get(context).quizPageIndex = r.nextInt(helperListToDeleteTheElement.length);
    paperEditingController?.text = "";
    Navigator.pop(context);
  }

  String stringValidator(String value) {
    value = value.trim();
    value = value.toLowerCase();
    value = value.replaceAll(' ', '');
    value = value.replaceAll('\n','');
    return value;
  }

  void playCorrectAnswerAudio() {
    AssetsAudioPlayer.newPlayer().open(
      Audio("assets/audios/correct_answer.mp3"),
      autoStart: true,
      showNotification: true,
    );
  }

  void playWrongAnswerAudio() {
    AssetsAudioPlayer.newPlayer().open(
      Audio("assets/audios/wrong_answer.mp3"),
      autoStart: true,
      showNotification: true,
    );
  }

  void showFinishQuizDialog(context) {
    AlertDialog alertDialog = AlertDialog(
      title: Center(
          child: Text(
              "Your Score is ${(numberOfCorrectAnswer * 100) / wordsFromThisGroup.length}%",
              style: Theme.of(context).textTheme.bodyText1)),
      backgroundColor: defaultColor1,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: defaultColor, width: 3),
        borderRadius: const BorderRadius.all(Radius.circular(25.0)),
      ),
      content: SizedBox(
        width: 75,
        height: 45,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Row(
              children: [
                const Spacer(),
                defaultButton(
                  background: defaultColor,
                  text: "Ok",
                  width: 100,
                  radius: 25,
                  function: () {
                    Navigator.pop(context);
                    resetQuizScreen(context);
                    navigateReplace(context,LayoutScreen());
                  },
                ),
                const Spacer(),
              ],
            )
          ],
        ),
      ),
    );
    showDialog(barrierDismissible:false,context: context, builder: (context) => alertDialog);
  }

  void explainTheQuizIdea(context){
    AlertDialog alertDialog = AlertDialog(
      title: Center(
          child: Text(
              "after pressing on ok you will be in the quiz screen , we make this screen to test yourself and show your memorize percentage for each group",
              style: Theme.of(context).textTheme.bodyText1)),
      backgroundColor: defaultColor1,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: defaultColor, width: 3),
        borderRadius: const BorderRadius.all(Radius.circular(25.0)),
      ),
      content: SizedBox(
        width: 75,
        height: 45,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Row(
              children: [
                const Spacer(),
                defaultButton(
                  background: defaultColor,
                  text: "Ok",
                  width: 100,
                  radius: 25,
                  function: () {
                    Navigator.pop(context);
                  },
                ),
                const Spacer(),
              ],
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (context) => alertDialog);
  }
}
