import 'dart:async';
import 'dart:io' show Platform;
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:flutter/foundation.dart';
import 'package:MemoryApp/layouts/layout_screen.dart';
import 'package:MemoryApp/models/word_model.dart';
import 'package:MemoryApp/shared/components/components.dart';
import 'package:MemoryApp/shared/cubit/cubit.dart';
import 'package:MemoryApp/shared/cubit/states.dart';
import 'package:MemoryApp/shared/notification_service/notification_service.dart';
import 'package:MemoryApp/shared/styles/color.dart';
import 'package:MemoryApp/flutter_text_to_voice_helper.dart';

import '../../components/components.dart';
import '../../models/group_model.dart';
import '../../shared/styles/icons.dart';
import '../add_group/add_group_screen.dart';

TextEditingController wordController = TextEditingController();
TextEditingController wordMeaningController = TextEditingController();
var formKey = GlobalKey<FormState>();
late FlutterTts flutterTts;

class WordScreen extends StatelessWidget {
  final GroupModel group;

  WordScreen({required this.group});

  NotificationService notificationService = NotificationService();
  List<WordModel> wordsFromThisGroup = [];

  @override
  Widget build(BuildContext context) {
    print("edit word screen");
    notificationService.initNotification();
    var cubit = LanguageAppCubit.get(context);
    return BlocConsumer<LanguageAppCubit, LanguageAppStates>(
      listener: (context, state) {},
      builder: (context, state) {
        wordsFromThisGroup = [];
        for (int i = 0; i < cubit.words.length; ++i) {
          if (cubit.words[i]?.groupId == group.groupId) {
            wordsFromThisGroup.add(cubit.words[i]!);
          }
            print("groupid=${group.groupId}");
        }
        print("edit word screen1");
        return Scaffold(
            appBar: AppBar(
              titleSpacing: 20,
              centerTitle: false,
              toolbarHeight: 56,
              title: Text(group.groupName!),
              /*actions: [
                IconButton(
                    onPressed: () {
                      showEditAndDeleteGroupDialog(context);
                    },
                    icon: const Icon(Icons.more_vert))
              ],*/
            ),
            body: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        Image(
                          image: AssetImage((selectImage1(group.groupCategory!))),
                          fit: BoxFit.cover,
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) =>
                              buildWordsItem(context, index),
                          //separatorBuilder: (BuildContext context, int index)=>divider(),
                          itemCount: wordsFromThisGroup.length,
                        ),
                      ],
                    )),
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                showAddWordDialog(context);
              }));

      },
    );
  }

  void showAddWordDialog(context) {
    var cubit = LanguageAppCubit.get(context);
    AlertDialog alertDialog = AlertDialog(
      title: Center(
          child:
              Text("New Word", style: Theme.of(context).textTheme.bodyText1)),
      backgroundColor: defaultColor1,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: defaultColor, width: 3),
        borderRadius: const BorderRadius.all(Radius.circular(25.0)),
      ),
      content: SizedBox(
        width: 400,
        height: 300,
        child: Form(
          key: formKey,
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                "Please enter the word that you want to memorize with its meaning  ",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.copyWith(fontWeight: FontWeight.w400, fontSize: 15),
              ),
              const Spacer(),
              defaultFromField(
                  controller: wordController,
                  type: TextInputType.text,
                  validate: (String? value) {
                    if(value!.isEmpty||value.trim().isEmpty) return "word must not be empty";
                    return null;
                  },
                  label: "Enter new word",
                  prefix: Icons.language_sharp,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(fontSize: 12)),
              const SizedBox(
                height: 20,
              ),
              defaultFromField(
                  controller: wordMeaningController,
                  type: TextInputType.text,
                  validate: (String? value) {
                    if ((value!.isEmpty||value.trim().isEmpty)) {
                      return "word meaning  must not be empty";
                    }
                    return null;
                  },
                  label: "Enter word meaning ",
                  prefix: Icons.abc_rounded,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(fontSize: 12)),
              const Spacer(),
              Row(
                children: [
                  defaultButton(
                    function: () {
                      clearAddWordDialog(context);
                    },
                    background: defaultColor,
                    text: "Cancel",
                    width: 100,
                    radius: 25,
                  ),
                  const Spacer(),
                  defaultButton(
                    function: () {
                      if (formKey.currentState!.validate()) {
                        showTheNotification();
                        WordModel newWord = WordModel(
                            word: wordController.text,
                            wordMeaning: wordMeaningController.text,
                            groupId: group.groupId);
                        cubit.insertIntoWordsTable(wordModel: newWord);
                        clearAddWordDialog(context);
                      }
                    },
                    background: defaultColor,
                    text: "Next",
                    width: 100,
                    radius: 25,
                  ),
                ],
              ),

            ],
          ),
        ),
      ),
    );
    showDialog(context: context,barrierDismissible: false, builder: (context) => alertDialog);
  }

  void deleteWordDialog(context, index) {
    var cubit = LanguageAppCubit.get(context);
    AlertDialog alertDialog = AlertDialog(
      title: Center(
          child: Text("Are you sure you want to delete this word ?",
              style: Theme.of(context).textTheme.bodyText1)),
      backgroundColor: defaultColor1,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: defaultColor, width: 3),
        borderRadius: const BorderRadius.all(Radius.circular(25.0)),
      ),
      content: SizedBox(
        width: 75,
        height: 45,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Row(
              children: [
                defaultButton(
                  background: defaultColor,
                  text: "No",
                  width: 100,
                  radius: 25,
                  function: () {
                    Navigator.pop(context);
                  },
                ),
                const Spacer(),
                defaultButton(
                  background: defaultColor,
                  text: "Yes",
                  width: 100,
                  radius: 25,
                  function: () {
                    cubit.deleteFromWordsTable(
                      id: wordsFromThisGroup[index].wordId,
                    );
                    Navigator.pop(context);
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (context) => alertDialog);
  }

  void ensureEditWordDialog(context, index, editWordController, editWordMeaningController){
    var cubit = LanguageAppCubit.get(context);
    AlertDialog alertDialog = AlertDialog(
      title: Center(
          child: Text("Are you sure that you want to save this change ?",
              style: Theme.of(context).textTheme.bodyText1)),
      backgroundColor: defaultColor1,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: defaultColor, width: 3),
        borderRadius: const BorderRadius.all(Radius.circular(25.0)),
      ),
      content: SizedBox(
        width: 75,
        height: 45,
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Spacer(),
                Row(
                  children: [
                    defaultButton(
                      background: defaultColor,
                      text: "No",
                      width: 100,
                      radius: 25,
                      function: () {
                        Navigator.pop(context);
                        cubit.changeEditState(wordsFromThisGroup[index].wordId,
                            wordsFromThisGroup[index].inEdit);
                      },
                    ),
                    const Spacer(),
                    defaultButton(
                      background: defaultColor,
                      text: "Yes",
                      width: 100,
                      radius: 25,
                      function: () {
                         cubit.updateWordTableData(
                             id: wordsFromThisGroup[index].wordId,
                             word: editWordController.text,
                             wordMeaning: editWordMeaningController.text,
                             inEdit: 0);
                         Navigator.pop(context);

                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ),

    );
    showDialog(context: context, builder: (context) => alertDialog);
  }

  void explainingHowToEditDialog(context, index) {
    var cubit = LanguageAppCubit.get(context);
    AlertDialog alertDialog = AlertDialog(
      title: Center(
          child: Text(
              "Now you can edit the words , just press on save icon when you finish to save your adjustment",
              style: Theme.of(context).textTheme.bodyText1)),
      backgroundColor: defaultColor1,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: defaultColor, width: 3),
        borderRadius: const BorderRadius.all(Radius.circular(25.0)),
      ),
      content: SizedBox(
        width: 75,
        height: 45,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Row(
              children: [
                const Spacer(),
                defaultButton(
                  background: defaultColor,
                  text: "Ok",
                  width: 100,
                  radius: 25,
                  function: () {
                    Navigator.pop(context);
                    cubit.changeEditState(wordsFromThisGroup[index].wordId,
                        wordsFromThisGroup[index].inEdit);
                  },
                ),
                const Spacer(),
              ],
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (context) => alertDialog);
  }

  void clearAddWordDialog(context) {
    Navigator.pop(context);
    wordController.text = "";
    wordMeaningController.text = "";
  }

  Widget buildWordsItem(context, int index) {
    TextEditingController editWordController = TextEditingController(text: wordsFromThisGroup[index].word);
    TextEditingController editWordMeaningController = TextEditingController(text: wordsFromThisGroup[index].wordMeaning);
    return BlocConsumer<LanguageAppCubit, LanguageAppStates>(
      listener: (BuildContext context, Object? state) {},
      builder: (BuildContext context, state) {
        LanguageAppCubit cubit = LanguageAppCubit.get(context);
        var editKey=GlobalKey<FormState>();
        return Padding(
          padding: const EdgeInsets.all(15),
          child: Card(
            shadowColor: defaultColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25),
                side: BorderSide(color: defaultColor, width: 2)),
            semanticContainer: false,
            color: defaultColor2,
            elevation: 20,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: SizedBox(
                  width: double.infinity,
                  child: Form(
                    key: editKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            const SizedBox(
                              width: 18,
                            ),
                            Expanded(
                              child: SizedBox(
                                child: TextFormField(
                                  enableInteractiveSelection: true,
                                  focusNode: FocusNode(),
                                  controller: editWordController,
                                  cursorColor: defaultColor,
                                  readOnly: (wordsFromThisGroup[index].inEdit == 0),
                                  validator: (String? value){
                                    if(value!.isEmpty||value.trim().isEmpty){
                                      return "Word must not be Empty";
                                    }
                                    return null;
                                  },
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(fontSize: 26),
                                  decoration:  const InputDecoration(
                                    border: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    focusedErrorBorder:InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                  ),
                                ),
                              ),
                            ),
                            if (wordsFromThisGroup[index].inEdit == 1)
                              IconButton(
                                onPressed: () {
                                  if(editKey.currentState!.validate()){
                                    ensureEditWordDialog(
                                        context,
                                        index,
                                        editWordController,
                                        editWordMeaningController);
                                  }

                                },
                                icon: Icon(Icons.save),
                              ),
                          ],
                        ),
                        const SizedBox(
                          height: 3,
                        ),
                        divider(),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                IconButton(
                                  onPressed: () {
                                    FlutterTextToVoiceHelper.flutterTts?.speak("${wordsFromThisGroup[index].word}  ${wordsFromThisGroup[index].wordMeaning}");
                                  },
                                  icon: const ImageIcon(
                                    AssetImage("assets/images/speaker.png"),
                                    size: 24,
                                  ),
                                ),
                                IconButton(
                                    onPressed: () {
                                      cubit.changeFavoriteState(
                                          wordsFromThisGroup[index].wordId,
                                          wordsFromThisGroup[index].inFavorite);
                                    },
                                    icon: Icon(
                                        (wordsFromThisGroup[index].inFavorite ==
                                                1)
                                            ? Icons.favorite
                                            : Icons.favorite_border_outlined)),
                                IconButton(
                                    onPressed: () {
                                      if (wordsFromThisGroup[index].inEdit == 0)
                                        explainingHowToEditDialog(context, index);
                                      //cubit.changeEditState(cubit.words[index]?.wordId,cubit.words[index]?.inEdit);
                                    },
                                    icon: Icon(Icons.edit)),
                                IconButton(
                                    onPressed: () {
                                      deleteWordDialog(context, index);
                                      //cubit.deleteFromWordsTable(id: wordsFromThisGroup[index]['word_id'],);
                                    },
                                    icon: Icon(
                                      Icons.delete,
                                    ))
                              ],
                            ),
                            divider1(),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 0, bottom: 15, left: 15, right: 15),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      child: TextFormField(

                                        focusNode: FocusNode(),
                                        controller: editWordMeaningController,
                                        cursorColor: defaultColor,
                                        validator: (String? value){
                                          if((value!.isEmpty||value.trim().isEmpty)){
                                            return "Word meaning must not be empty";
                                          }
                                          return null;
                                        },
                                        decoration:  const InputDecoration(
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          focusedErrorBorder:InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                        ),
                                        readOnly:
                                            (wordsFromThisGroup[index].inEdit ==
                                                0),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 16,
                                              color: defaultColor,
                                            ),
                                        minLines: 1,
                                        maxLines: 500,
                                        toolbarOptions: ToolbarOptions(
                                          paste: true,
                                          cut: true,
                                          copy: true,
                                          selectAll: true,
                                        ),
                                      ),
                                      width: double.infinity,
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )),
            ),
          ),
        );
      },
    );
  }

  void showTheNotification(){
    notificationService.showNotification(
        1,
        wordController.text,
        wordMeaningController.text,
        const Duration(seconds: 2));
    notificationService.showNotification(
        2,
        wordController.text,
        wordMeaningController.text,
        const Duration(minutes: 10));
    notificationService.showNotification(
        3,
        wordController.text,
        wordMeaningController.text,
        const Duration(hours: 24,minutes: 10));
    notificationService.showNotification(
        4,
        wordController.text,
        wordMeaningController.text,
        const Duration(days: 7,hours: 24,minutes: 10));
    notificationService.showNotification(
        5,
        wordController.text,
        wordMeaningController.text,
        const Duration(days:37,hours: 24,minutes: 10));
    notificationService.showNotification(
        6,
        wordController.text,
        wordMeaningController.text,
        const Duration(days:217,hours: 24,minutes: 10));
  }
}

//card fore edition
