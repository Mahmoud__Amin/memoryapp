import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import '../../components/components.dart';
import '../../shared/components/components.dart';
import '../../shared/network/local/cache_helper.dart';
import '../../shared/styles/color.dart';
import '../login/login_screen.dart';

class PageBoardingModels {
  final String image;
  final String title;
  final String body;
  final double widthBeforeTheContainer;
  final double heightBeforeTheContainer;

  PageBoardingModels(this.image, this.title, this.body,
      this.heightBeforeTheContainer, this.widthBeforeTheContainer);
}

List<PageBoardingModels> boarding = [
  PageBoardingModels(
      'assets/images/onBoarding1.jpg',
      'EXPLORE',
      'Our brand new app to help you memorize vocabs, numbers, and much more',
      150,
      50),
  PageBoardingModels(
      'assets/images/onBoarding2.jpg',
      'MEMORIZE',
      'What ever you want based on science research technique tried by thousands around the world',
      300,
      30),
  PageBoardingModels(
      'assets/images/onBoarding3.jpg',
      'ENJOY',
      'Learning with interactive quizzes and a simplified interface ready for you to use',
      400,
      15)
];
bool isLast = false;
PageController pageController = PageController();

class OnBoardingScreen extends StatefulWidget {
  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  void onSubmit() {
    CacheHelper.setData(key: "OnBoarding", value: true);
    navigateReplace(context, LoginScreen());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 0,
        ),
        body: Stack(
          children: [
            PageView.builder(
                itemBuilder: (BuildContext context, int index) => buildBoardingItem(context, boarding[index]),
                itemCount: boarding.length,
                controller: pageController,
                physics: const BouncingScrollPhysics(),
                onPageChanged: (int index) {
                  if (index == boarding.length - 1) {
                    setState(() {
                      isLast = true;
                    });
                  } else {
                    setState(() {
                      isLast = false;
                    });
                  }
                }),
            Column(
              children: [
                Row(
                  children: [
                    const Spacer(),
                    MaterialButton(
                      onPressed: onSubmit,
                      child: Row(
                        children: [
                          Text(
                            "skip",
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                ?.copyWith(color: defaultColor1),
                          ),
                          const SizedBox(
                            width: 2,
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 15,
                            color: defaultColor1,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                Row(
                  children: [
                    const Spacer(),
                    SmoothPageIndicator(
                        controller: pageController,
                        count: boarding.length,
                        effect: CustomizableEffect(
                            activeDotDecoration: DotDecoration(
                                width: 18,
                                height: 18,
                                color: defaultColor,
                                borderRadius: BorderRadius.circular(16)),
                            dotDecoration: DotDecoration(
                                color: defaultColor2,
                                borderRadius: BorderRadius.circular(16),
                                width: 15,
                                height: 15),
                            spacing: 10)),

                    const Spacer()
                  ],
                ),
                const SizedBox(
                  height: 15,
                )
              ],
            ),

          ],
        ));
  }

  Widget buildBoardingItem(BuildContext context, PageBoardingModels model) {
    return Stack(children: [
      Container(
        width: double.infinity,
        height: double.infinity,
        decoration:  BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage(model.image)),
        ),
      ),
      Column(
        children: [
          SizedBox(height: model.heightBeforeTheContainer),
          Row(
            children: [
               SizedBox(
                width: model.widthBeforeTheContainer,
              ),
               Container(
                padding: EdgeInsets.symmetric(horizontal: 5),
                height: 150,
                width: 250,
                color: defaultColor.withOpacity(0.5),
                child: Column(
                  children: [
                    Text(
                      model.title,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          ?.copyWith(color: defaultColor2, fontSize: 35),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      model.body,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          ?.copyWith(color: defaultColor2, fontSize: 17),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
              const Spacer()
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            height: 40,
            width: 60,
            color: defaultColor.withOpacity(0.5),
            child: MaterialButton(
              onPressed: () {
                if (isLast) {
                  onSubmit();
                } else {
                  pageController.nextPage(
                      duration: const Duration(milliseconds: 750),
                      curve: Curves.fastLinearToSlowEaseIn);
                }
              },
              child: Icon(Icons.arrow_forward, size: 22, color: defaultColor1),
            ),
          ),

        ],
      )
    ]);
  }

}
