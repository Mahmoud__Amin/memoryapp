import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:MemoryApp/shared/cubit/cubit.dart';
import 'package:MemoryApp/shared/cubit/states.dart';

import '../../components/components.dart';
import '../../shared/components/components.dart';
import '../../shared/components/constans.dart';
import '../../shared/styles/color.dart';


class SettingsScreen extends StatelessWidget {
  var formKey = GlobalKey<FormState>();
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LanguageAppCubit,LanguageAppStates>(
      listener: (BuildContext context, state) {},
      builder: (BuildContext context, state) {
        var cubit = LanguageAppCubit.get(context).getUserModel;
        return ConditionalBuilder(
            condition: cubit?.data!=null,
            builder: (BuildContext context) {
              nameController.text=cubit?.data?.name as String;
              emailController.text = cubit?.data?.email as String;
              phoneController.text=cubit?.data?.phone as String;
              return Padding(
                padding: const EdgeInsets.all(20),
                child: Form(
                  key: formKey,
                  child: Column(
                    children: [
                      if(state is LanguageAppLoadingEditUserDataState)const LinearProgressIndicator(),
                      const SizedBox(height: 20,),
                      defaultFromField(
                        controller: nameController,
                        type: TextInputType.name,
                        validate: (String? value) {
                          if (value!.isEmpty) {
                            return "Name must not be empty";
                          }
                          return null;
                        },
                        label: "Name",
                        prefix: Icons.person,
                        //isClickable: false,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      defaultFromField(
                        controller: emailController,
                        type: TextInputType.emailAddress,
                        validate: (String? value) {
                          if (value!.isEmpty) {
                            return "Email Addresses must not be empty";
                          }
                          return null;
                        },
                        label: "Email Address",
                        prefix: Icons.email,
                        //isClickable: false,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      defaultFromField(
                        controller: phoneController,
                        type: TextInputType.phone,
                        validate: (String? value) {
                          if (value!.isEmpty) {
                            return "Phone must not be empty";
                          }
                          return null;
                        },
                        label: "Phone",
                        prefix: Icons.phone,
                        //isClickable: false,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      defaultButton(
                        background: defaultColor,
                        radius: 25,
                        function:(){
                          if(formKey.currentState!.validate()){
                            LanguageAppCubit.get(context).updateUserData(
                              name: nameController.text,
                              email: emailController.text,
                              phone: phoneController.text,
                            );
                          }

                        },
                        text: "update",
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      defaultButton(
                        background: defaultColor,
                        radius: 25,

                        function: () => logOut(context),
                        text: "logout",
                      ),
                    ],
                  ),
                ),
              );
            },
            fallback: (context) => const Center(child: CircularProgressIndicator()));
      },
    );
  }
}
