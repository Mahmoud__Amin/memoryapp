import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:MemoryApp/modules/login/login_screen.dart';
import 'package:MemoryApp/shared/styles/color.dart';
import '../../components/components.dart';
import '../../layouts/layout_screen.dart';
import '../../shared/components/components.dart';
import '../../shared/components/constans.dart';
import '../../shared/cubit/cubit.dart';
import '../../shared/network/local/cache_helper.dart';
import 'cubit/cubit.dart';
import 'cubit/states.dart';
class RegisterScreen extends StatelessWidget {
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var nameController = TextEditingController();
  var phoneController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    var cubit =LanguageAppCubit.get(context);
    return BlocConsumer<RegisterCubit, RegisterStates>(
        listener: (BuildContext context, state) {
      if (state is RegisterSuccessState) {
        if (state.registerModel.status!) {
          CacheHelper.setData(
              key: 'token', value: state.registerModel.data?.token).then((value){
              token=CacheHelper.getDate(key: 'token');
              cubit.getUserData();
              navigateReplace(context, LayoutScreen());
          });
        }
        else {
          defaultToast(
              message: state.registerModel.message!, state: ToastStates.ERROR);
        }}},
        builder: (BuildContext context, state) {
          return Scaffold(
            appBar: AppBar(toolbarHeight:40),
            body: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(radius:35,backgroundColor:defaultColor,child: Icon(Icons.person,color: defaultColor1,size: 50,)),
                        const SizedBox(height: 20,),
                        Text(
                            "Personal Information",
                            style: Theme
                                .of(context)
                                .textTheme
                                .headline5
                                ?.copyWith(color: defaultColor,fontWeight: FontWeight.bold)
                        ),
                        const SizedBox(height: 20,),
                        defaultFromField(
                            controller: nameController,
                            type: TextInputType.name,
                            validate: (String? value) {
                              if(value!.isEmpty||value.trim().isEmpty) {
                                return "Name must not be empty";
                              }
                              return null;
                            },
                            label: "Full Name",
                            prefix: Icons.person),
                        const SizedBox(
                          height: 15,
                        ),
                        defaultFromField(
                            controller: emailController,
                            type: TextInputType.emailAddress,
                            validate: (String? value) {
                              if(value!.isEmpty||value.trim().isEmpty) {
                                return "Email Addresses must not be empty";
                              }
                              return null;
                            },
                            label: "Email",
                            prefix: Icons.email_outlined),
                        const SizedBox(
                          height: 15,
                        ),
                        defaultFromField(
                            controller: passwordController,
                            type: TextInputType.visiblePassword,
                            validate: (String? value) {
                              if(value!.isEmpty||value.trim().isEmpty) {
                                return "Password must not be empty";
                              }
                              return null;
                            },
                            label: "Password",
                            isPassword: RegisterCubit
                                .get(context)
                                .passwordIsShown,
                            suffix: RegisterCubit
                                .get(context)
                                .passwordIsShown
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                            suffixPressed: () =>
                                RegisterCubit.get(context)
                                    .changeLoginVisibilityIcon(),
                            prefix: Icons.lock_outline),
                        const SizedBox(
                          height: 15,
                        ),
                        defaultFromField(
                            controller: phoneController,
                            type: TextInputType.phone,
                            validate: (String? value) {
                              if(value!.isEmpty||value.trim().isEmpty) {
                                return "phone must not be empty";
                              }
                              return null;
                            },
                            label: "Phone",
                            prefix: Icons.phone),
                        const SizedBox(
                          height: 35,
                        ),
                        ConditionalBuilder(
                          condition: (state is! RegisterLoadingState),
                          builder: (context) =>
                              defaultButton(
                                text: "Sign Up",
                                background: defaultColor,
                                radius: 25,
                                function: () {
                                  if (formKey.currentState!.validate()) {
                                    RegisterCubit.get(context).userRegister(
                                        email: emailController.text,
                                        password: passwordController.text,
                                        name: nameController.text,
                                        phone: phoneController.text
                                    );
                                  }
                                },
                              ),
                          fallback: (context) =>
                              const Center(child: CircularProgressIndicator()),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Have an account before?",style:Theme.of(context).textTheme.bodyText1),
                              defaultTextButton(
                                  function: () {
                                    navigateReplace(
                                        context, LoginScreen());
                                  },
                                  color: defaultColor3,
                                  text: "Login")
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }

    );
  }
}
