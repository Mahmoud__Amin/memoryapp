
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:MemoryApp/modules/register/cubit/states.dart';


import '../../../models/login_model.dart';
import '../../../shared/network/end_points.dart';
import '../../../shared/network/remote/dio_helper.dart';

class RegisterCubit extends Cubit<RegisterStates>{
  RegisterCubit() : super(RegisterInitialState());
  static RegisterCubit get(context)=>BlocProvider.of(context);
  LoginModel? registerModel;
  bool passwordIsShown=true;
  void changeLoginVisibilityIcon(){
    passwordIsShown=!passwordIsShown;
    emit(RegisterChangePasswordVisibilityState());
  }

  void userRegister({
    required String email,
    required String password,
    required String name,
    required String phone,

  }){
    emit(RegisterLoadingState());
    DioHelper.postData(
      url:REGISTER,
      data:{
        'email':email,
        'password':password,
        'name':name,
        'phone':phone,
        'image':"https://student.valuxapps.com/storage/uploads/categories/16301438353uCFh.29118.jpg"
      },
    )?.then((value){
      registerModel=LoginModel.fromJson(value?.data);
      emit(RegisterSuccessState(registerModel!));
    }).catchError((error){
      emit(RegisterErrorState(error.toString()));
      print(error.toString());
    });
  }

}