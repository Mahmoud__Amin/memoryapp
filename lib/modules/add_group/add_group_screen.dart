import 'dart:ui';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:MemoryApp/models/group_model.dart';
import 'package:MemoryApp/modules/login/login_screen.dart';
import 'package:MemoryApp/shared/cubit/cubit.dart';
import 'package:MemoryApp/shared/cubit/states.dart';
import '../../components/components.dart';
import '../../shared/components/components.dart';
import '../../shared/styles/color.dart';
import '../word_screen/word_screen.dart';

final List<String> items = ["Words", "Numbers", "Information", "Others"];
TextEditingController groupCategoryController = TextEditingController();

List<String> GroupNames = [
  "Words",
  "Numbers",
  "Information",
  "Others",
];
TextEditingController groupNameController = TextEditingController();
  var formKey = GlobalKey<FormState>();

class AddGroupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var cubit =LanguageAppCubit.get(context);
    print(cubit.groups.toString());
    return buildAddGroupScreen(context);
  }
}

Widget buildAddGroupScreen(context) {
  return  BlocConsumer<LanguageAppCubit,LanguageAppStates>(
    listener: (BuildContext context, state) {  },
    builder: (BuildContext context, Object? state) {
      return SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
              children: [
                buildAddGroupItem(context),
                buildGridView(context,false),
              ]));
    },
  );
}

Widget buildAddGroupItem(context) {
  return InkWell(
      onTap: () {
         showAddGroupDialog(context);
      },
      child: Padding(
        padding: const EdgeInsets.all(30),
        child: SizedBox(
          width: double.infinity,
          height: 250,
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                height: 250,
                decoration: BoxDecoration(
                    color: defaultColor2,
                    borderRadius: BorderRadius.circular(25),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(4, 4),
                        color: defaultColor,
                        blurRadius: 5,
                        //spreadRadius: 5
                      ),
                    ],
                    border: Border.all(
                      color: defaultColor,
                      width: 3,
                    )),
              ),
              Center(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 25,
                    ),
                    CircleAvatar(
                      radius: 72,
                      backgroundColor: defaultColor,
                      child: CircleAvatar(
                        radius: 70,
                        child: Icon(
                          Icons.add,
                          color: defaultColor2,
                          size: 120,
                        ),
                        backgroundColor: defaultColor1,
                      ),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    Text(
                      "Add New Group",
                      style: Theme.of(context).textTheme.bodyText1,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ));
}

void clearAddGroupDialog(context){
  LanguageAppCubit.get(context).selectedValue=null;
  groupNameController.clear();
  Navigator.pop(context);
}

Widget buildDropDownButton(context) => BlocConsumer<LanguageAppCubit,LanguageAppStates>(
  listener: (BuildContext context, state) {  },
  builder: (BuildContext context, Object? state) {
    return Center(
      child: DropdownButtonHideUnderline(
        child: DropdownButton2 (
          isExpanded: true,
          hint: Row(
            children: [
              Icon(
                Icons.list,
                size: 16,
                color: defaultColor,
              ),
              const SizedBox(
                width: 4,
              ),
              Expanded(
                child: Text(
                  'Choose Group Category',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: defaultColor,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
          items: items.map((item) => DropdownMenuItem<String>(
            alignment: Alignment.center,
            value: item,
            child:Text(
              item,
              style: Theme.of(context).textTheme.bodyText1,
              overflow: TextOverflow.ellipsis,
            ),
          ))
              .toList(),
          value: LanguageAppCubit.get(context).selectedValueInEdited ,
          onChanged: (value) {
            LanguageAppCubit.get(context).updateDropDownButtonItem(value);
          },
          icon: const Icon(
            Icons.arrow_forward_ios_outlined,
          ),
          iconSize: 14,
          iconEnabledColor: defaultColor,
          buttonHeight: 60,
          buttonWidth: double.infinity,
          buttonPadding: const EdgeInsets.only(left: 14, right: 14),
          buttonDecoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            border: Border.all(color: defaultColor, width: 2.5),
            color: defaultColor1,
          ),
          buttonElevation: 2,
          itemHeight: 40,
          itemPadding: const EdgeInsets.only(left: 14, right: 14),
          dropdownMaxHeight: 200,
          dropdownWidth: 200,
          dropdownPadding: null,
          dropdownDecoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            color: defaultColor1,
          ),
          dropdownElevation: 8,
          scrollbarRadius: const Radius.circular(40),
          scrollbarThickness: 6,
          scrollbarAlwaysShow: true,
          offset: const Offset(90, 40),
        ),
      ),
    );
  },
);

Widget buildDropDownButtonForNewGroup(context) => BlocConsumer<LanguageAppCubit, LanguageAppStates>(
      listener: (BuildContext context, state) {},
      builder: (BuildContext context, Object? state) {
        return Center(
          child: DropdownButtonHideUnderline(
            child: DropdownButton2(
              isExpanded: true,
              hint: Row(
                children: [
                  Icon(
                    Icons.list,
                    size: 16,
                    color: defaultColor,
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Expanded(
                    child: Text(
                      'Choose Group Category',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: defaultColor,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              items: items.map((item) => DropdownMenuItem<String>(
                alignment: Alignment.center,
                value: item,
                child: Text(
                  item,
                  style: Theme.of(context).textTheme.bodyText1,
                  overflow: TextOverflow.ellipsis,
                ),
              ))
                  .toList(),
              value: LanguageAppCubit.get(context).selectedValue,
              onChanged: (value) {
                LanguageAppCubit.get(context).chooseDropDownButtonItem(value);
              },
              icon: const Icon(
                Icons.arrow_forward_ios_outlined,
              ),
              iconSize: 14,
              iconEnabledColor: defaultColor,
              buttonHeight: 60,
              buttonWidth: double.infinity,
              buttonPadding: const EdgeInsets.only(left: 14, right: 14),
              buttonDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                border: Border.all(color: defaultColor, width: 2.5),
                color: defaultColor1,
              ),
              buttonElevation: 2,
              itemHeight: 40,
              itemPadding: const EdgeInsets.only(left: 14, right: 14),
              dropdownMaxHeight: 200,
              dropdownWidth: 200,
              dropdownPadding: null,
              dropdownDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: defaultColor1,
              ),
              dropdownElevation: 8,
              scrollbarRadius: const Radius.circular(40),
              scrollbarThickness: 6,
              scrollbarAlwaysShow: true,
              offset: const Offset(90, 40),
            ),
          ),
        );
      },
    );

void showAddGroupDialog(context){
  var cubit=LanguageAppCubit.get(context);
  AlertDialog alertDialog = AlertDialog(
    title: Center(
        child:
        Text("New Group", style: Theme.of(context).textTheme.bodyText1)),
    backgroundColor: defaultColor1,
    shape: RoundedRectangleBorder(
      side: BorderSide(color: defaultColor, width: 3),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    content: Container(
      width: 600,
      height: 300,
      child: Form(
        key: formKey,
        child: Column(
          children: [
            Text(
              "Please enter your group Name,And Choose its Category",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  ?.copyWith(fontWeight: FontWeight.w400, fontSize: 15),
            ),
            const SizedBox(
              height: 50,
            ),
            defaultFromField(
                prefix: Icons.add_circle,
                controller: groupNameController,
                type: TextInputType.text,
                validate: (String? value) {
                  if(value!.isEmpty||value.trim().isEmpty) return "Group name must not be empty";
                  return null;
                },
                label: "Enter Group Name",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.copyWith(fontSize: 13)),
            const SizedBox(
              height: 20,
            ),
            buildDropDownButtonForNewGroup(context),
            const Spacer(),
            Row(
              children: [
                defaultButton(
                  function: () {
                    clearAddGroupDialog(context);
                  },
                  background: defaultColor,
                  text: "Cancel",
                  width: 100,
                  radius: 25,),
                const Spacer(),
                defaultButton(
                  function: () {
                    if(formKey.currentState!.validate()) {
                     cubit.selectedValue ??= "Others";
                     GroupModel group=GroupModel(groupName: groupNameController.text, groupCategory: cubit.selectedValue!);
                     cubit.insertIntoGroupsTable(groupModel:group);
                      clearAddGroupDialog(context);
                    }
                  },
                  background: defaultColor,
                  text: "Next",
                  width: 100,
                  radius: 25,
                ),
              ],
            )
          ],
        ),
      ),
    ),
  );
  showDialog(context: context, builder: (context) => alertDialog,barrierDismissible:false);
}

void deleteGroupDialog(context,group) {
  var cubit = LanguageAppCubit.get(context);
  AlertDialog alertDialog = AlertDialog(
    title: Center(
        child: Text("Are you sure you want to delete this group ?",
            style: Theme.of(context).textTheme.bodyText1)),
    backgroundColor: defaultColor1,
    shape: RoundedRectangleBorder(
      side: BorderSide(color: defaultColor, width: 3),
      borderRadius: const BorderRadius.all(Radius.circular(25.0)),
    ),
    content: SizedBox(
      width: 75,
      height: 45,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Spacer(),
          Row(
            children: [
              defaultButton(
                background: defaultColor,
                text: "No",
                width: 100,
                radius: 25,
                function: () {
                  Navigator.pop(context);
                },
              ),
              const Spacer(),
              defaultButton(
                background: defaultColor,
                text: "Yes",
                width: 100,
                radius: 25,
                function: () {
                  cubit.deleteFromGroupsTable(id: group.groupId);
                  Navigator.pop(context);
                },
              ),
            ],
          )
        ],
      ),
    ),
  );
  showDialog(context: context, builder: (context) => alertDialog);
}

void showEditAndDeleteGroupDialog(context,group) {
  var cubit = LanguageAppCubit.get(context);
  AlertDialog alertDialog = AlertDialog(
    alignment:Alignment.bottomCenter,
    backgroundColor: defaultColor2,
    titlePadding: EdgeInsets.zero,
    contentPadding: EdgeInsets.zero,
    insetPadding: const EdgeInsets.only(left: 100, right: 100, top: 0, bottom: 200),
    content: SizedBox(
      width: 200,
      height: 80,
      child: Column(
        children: [
          Container(
            width: 200,
            height: 40,
            decoration: BoxDecoration(
                border: Border.all(
                  color: defaultColor,
                  width: 1.5,
                )),
            child: MaterialButton(
              onPressed: () {
                Navigator.pop(context);
                showEditGroupDialog(context,group);
              },
              child: Row(
                children: [
                  Icon(
                    Icons.edit,
                    color: defaultColor,
                    size: 25,
                  ),
                  const SizedBox(
                    width: 3,
                  ),
                  Text(
                    "Edit group ",
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: 200,
            height: 40,
            decoration: BoxDecoration(
                border: Border.all(
                  color: defaultColor,
                  width: 1.5,
                )),
            child: MaterialButton(
              onPressed: () {
                Navigator.pop(context);
                deleteGroupDialog(context,group);
              },
              child: Row(
                children: [
                  Icon(
                    Icons.delete,
                    color: defaultColor,
                    size: 25,
                  ),
                  const SizedBox(
                    width: 3,
                  ),
                  Text(
                    "Delete group ",
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(fontSize: 15),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    ),
  );
  showDialog(context: context, builder: (context) => alertDialog);
}

void showEditGroupDialog(context,group) {
  var cubit = LanguageAppCubit.get(context);
  cubit.selectedValueInEdited = group.groupCategory;
  print("From Add group we are gonna see if there is  a value for group category");
  cubit.printGroupModel(group);
  var formKey = GlobalKey<FormState>();
  TextEditingController groupNameController = TextEditingController(text: group.groupName);
  AlertDialog alertDialog = AlertDialog(
    //title: Center(child: Text("Update group information", style: Theme.of(context).textTheme.bodyText1?.copyWith(fontSize: 15))),
    backgroundColor: defaultColor1,
    shape: RoundedRectangleBorder(
      side: BorderSide(color: defaultColor, width: 3),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    content: Container(
      width: 600,
      height: 250,
      child: Form(
        key: formKey,
        child: Column(
          children: [
            const Spacer(),
            defaultFromField(
                prefix: Icons.add_circle,
                controller: groupNameController,
                type: TextInputType.text,
                validate: (String? value) {
                  if(value!.isEmpty||value.trim().isEmpty) return "Group name must not be empty";
                  return null;
                },
                label: "",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.copyWith(fontSize: 13)),
            const SizedBox(
              height: 20,
            ),
            buildDropDownButton(context),
            const Spacer(),
            Row(
              children: [
                defaultButton(
                  function: () {
                    clearAddGroupDialog(context);
                  },
                  background: defaultColor,
                  text: "Cancel",
                  width: 100,
                  radius: 25,
                ),
                const Spacer(),
                defaultButton(
                  function: () {
                    if (formKey.currentState!.validate()) {
                      cubit.updateGroupsTableData(
                          groupCategory: cubit.selectedValueInEdited,
                          groupName: groupNameController.text,
                          id: group.groupId);
                      GroupModel newGroup=GroupModel(groupId:group.groupId,groupName: groupNameController.text, groupCategory:cubit.selectedValueInEdited);
                      cubit.printGroupModel(newGroup);
                      Navigator.pop(context);
                      //navigateReplace(context,WordScreen(group: newGroup));
                    }
                  },
                  background: defaultColor,
                  text: "Save",
                  width: 100,
                  radius: 25,
                ),
              ],
            )
          ],
        ),
      ),
    ),
  );
  showDialog(context: context, builder: (context) => alertDialog);
}



