import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:MemoryApp/layouts/layout_screen.dart';
import 'package:MemoryApp/shared/cubit/cubit.dart';
import 'package:MemoryApp/shared/styles/color.dart';
import '../../components/components.dart';

import '../../shared/components/components.dart';
import '../../shared/components/constans.dart';
import '../../shared/network/local/cache_helper.dart';
import '../register/register_screen.dart';
import 'cubit/cubit.dart';
import 'cubit/states.dart';

class LoginScreen extends StatelessWidget {
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var cubit =LanguageAppCubit.get(context);
    return BlocConsumer<LoginCubit, LoginStates>(
        listener: (BuildContext context, state) {
          if (state is LoginSuccessState) {
            if (state.loginModel.status==true) {
              CacheHelper.setData(
                key: 'token', value: state.loginModel.data?.token).then((value){
                token=CacheHelper.getDate(key: 'token');
                print("Token from the login screen$token");
                cubit.getUserData();
                navigateReplace(context, LayoutScreen());
              });
            }
            else {
              defaultToast(message: state.loginModel.message!, state: ToastStates.ERROR);
            }
          }
        },
        builder: (BuildContext context, state) {
          return Scaffold(
            appBar: AppBar(title: const Text("Memory App"),),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 120,),
                      defaultFromField(
                          controller: emailController,
                          type: TextInputType.emailAddress,
                          validate: (String? value) {
                            if(value!.isEmpty||value.trim().isEmpty) {
                              return "Email Addresses must not be empty";
                            }
                            return null;
                          },
                          label: "Email Address",
                          prefix: Icons.email_outlined),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultFromField(
                          controller: passwordController,
                          type: TextInputType.visiblePassword,
                          validate: (String? value) {
                            if(value!.isEmpty||value.trim().isEmpty) {
                              return "Password must not be empty";
                            }
                            return null;
                          },
                          label: "Password",
                          isPassword: LoginCubit
                              .get(context)
                              .passwordIsShown,
                          onSubmit: (value) {
                            if (formKey.currentState!.validate()) {
                              LoginCubit.get(context).userLogin(
                                  email: emailController.text,
                                  password: passwordController.text);
                            }
                          },
                          suffix: LoginCubit
                              .get(context)
                              .passwordIsShown
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                          suffixPressed: () =>
                              LoginCubit.get(context)
                                  .changeLoginVisibilityIcon(),
                          prefix: Icons.lock_outline),
                      const SizedBox(
                        height: 50,
                      ),
                      ConditionalBuilder(
                        condition: (state is! LoginLoadingState),
                        builder: (context) =>
                            defaultButton(
                              background: defaultColor,
                              radius: 25,
                              text: "login",
                              function: () {
                                if (formKey.currentState!.validate()) {
                                  LoginCubit.get(context).userLogin(
                                      email: emailController.text,
                                      password: passwordController.text);
                                    // navigateReplace(context, LayoutScreen());
                                }
                              },
                            ),
                        fallback: (context) =>
                        const Center(child: CircularProgressIndicator()),
                      ),
                      const SizedBox(
                        height: 70,
                      ),
                      Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                             Text("Don't have an account?",style:Theme.of(context).textTheme.bodyText1),
                             defaultTextButton(
                                function: () {
                                  navigateReplace(
                                      context, RegisterScreen());
                                },
                                color: defaultColor3,
                                text: "sign up")
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        }

    );
  }
}
