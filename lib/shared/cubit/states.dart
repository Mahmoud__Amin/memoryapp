
abstract class LanguageAppStates{}

class LanguageAppInitialState extends LanguageAppStates{}

class LanguageAppChangeBottomNavigationBarState extends LanguageAppStates{}

class LanguageAppChangeBottomSheetState extends LanguageAppStates{}

class LanguageAppInsertIntoGroupsTableState extends LanguageAppStates{}

class LanguageAppInsertIntoWordsTableState extends LanguageAppStates{}

class LanguageAppDeleteFromWordsTableLoadingState extends LanguageAppStates{}

class LanguageAppDeleteFromWordsTableSuccessState extends LanguageAppStates{}

class LanguageAppDeleteFromWordsTableErrorState extends LanguageAppStates{
  final String error;
  LanguageAppDeleteFromWordsTableErrorState(this.error);
}

class LanguageAppDeleteFromGroupsTableLoadingState extends LanguageAppStates{}

class LanguageAppDeleteFromGroupsTableSuccessState extends LanguageAppStates{}

class LanguageAppDeleteFromGroupsTableErrorState extends LanguageAppStates{
  final String error;
  LanguageAppDeleteFromGroupsTableErrorState(this.error);
}

class LanguageAppUpdateWordsTableLoadingState extends LanguageAppStates{}

class LanguageAppUpdateWordsTableSuccessState extends LanguageAppStates{}

class LanguageAppUpdateWordsTableErrorState extends LanguageAppStates{
  final String error;
  LanguageAppUpdateWordsTableErrorState(this.error);
}
class LanguageAppUpdateGroupsTableLoadingState extends LanguageAppStates{}

class LanguageAppUpdateGroupsTableSuccessState extends LanguageAppStates{}

class LanguageAppUpdateGroupsTableErrorState extends LanguageAppStates{
  final String error;
  LanguageAppUpdateGroupsTableErrorState(this.error);
}
class LanguageAppChangeWordEditionState extends LanguageAppStates{}

class LanguageAppChangeFavoriteIconState extends LanguageAppStates{}

class LanguageAppCreateDatabaseState extends LanguageAppStates{}

class LanguageAppGetGroupsTableDataLoadingState extends LanguageAppStates{}

class LanguageAppGetGroupsTableDataSuccessState extends LanguageAppStates{}

class LanguageAppGetWordsTableDataLoadingState extends LanguageAppStates{}

class LanguageAppGetWordsTableDataSuccessState extends LanguageAppStates{}

class LanguageAppChooseDropDownButtonItemState extends LanguageAppStates{}

class LanguageAppGoToNextQuestionState extends LanguageAppStates{}

class LanguageAppLoadingEditUserDataState extends LanguageAppStates{}

class LanguageAppSuccessEditUserDataState extends LanguageAppStates{}

class LanguageAppAppErrorEditUserState extends LanguageAppStates{
  final String error;
  LanguageAppAppErrorEditUserState(this.error);
}
class LanguageAppLoadingGetUserDataState extends LanguageAppStates{}

class LanguageAppSuccessGetUserDataState extends LanguageAppStates{}

class LanguageAppErrorGetUserState extends LanguageAppStates{
  final String error;
  LanguageAppErrorGetUserState(this.error);
}





