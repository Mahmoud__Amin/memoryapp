import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:MemoryApp/models/word_model.dart';
import 'package:MemoryApp/modules/settings/settings_screen.dart';
import 'package:MemoryApp/shared/cubit/states.dart';
import 'package:sqflite/sqflite.dart';
import '../../models/group_model.dart';
import '../../models/login_model.dart';
import '../../modules/add_group/add_group_screen.dart';
import '../../modules/favorite_screen/favorite_screen.dart';
import '../../modules/review_session/review_session_screen.dart';
import '../components/constans.dart';
import '../network/end_points.dart';
import '../network/remote/dio_helper.dart';

class LanguageAppCubit extends Cubit<LanguageAppStates> {
  LanguageAppCubit() : super(LanguageAppInitialState());

  static LanguageAppCubit get(context) => BlocProvider.of(context);
  int currentIndex = 1;
  List<Widget> bodyList = [
    ReviewSessionScreen(),
    AddGroupScreen(),
    FavoriteScreen(),
    SettingsScreen()
  ];
  List<BottomNavigationBarItem> items = [
    const BottomNavigationBarItem(icon: Icon(Icons.reviews_outlined), label: "Review"),
    const BottomNavigationBarItem(icon: Icon(Icons.add), label: "Add Group"),
    const BottomNavigationBarItem(icon: Icon(Icons.star), label: "Favorite"),
    const BottomNavigationBarItem(icon: Icon(Icons.settings), label: "Settings"),
  ];

  void changeBottomNavigationBarIndex(int index) {
    currentIndex = index;
    emit(LanguageAppChangeBottomNavigationBarState());
  }

  bool isBottomSheetShown = false;
  IconData floatingActionButtonIcon = Icons.edit;

  void changeBottomSheetState({required bool isShow, required IconData icon}) {
    isBottomSheetShown = isShow;
    floatingActionButtonIcon = icon;
    emit(LanguageAppChangeBottomSheetState());
  }

  String? selectedValue;

  String? selectedValueInEdited;

  void chooseDropDownButtonItem(value) {
    selectedValue = value as String;
    emit(LanguageAppChooseDropDownButtonItemState());
  }
  void updateDropDownButtonItem(value){
    selectedValueInEdited=value as String;
    emit(LanguageAppChooseDropDownButtonItemState());
  }

  Database? database;
  List<GroupModel?> groups =[];
  List<WordModel?>  words  =[];

  static Future _onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }
  Future<void> createDataBase() async {
    openDatabase('memory.db', version: 1, onConfigure: _onConfigure,
        onCreate: (database, version) {
      print('database created');
      database.execute(
          'CREATE TABLE groups (group_id INTEGER PRIMARY KEY autoincrement,group_name TEXT, group_category TEXT)')
          .then((value) {print('groups\'Table Created');})
          .catchError((error) {print('Error When Creating group\'s Table ${error.toString()}');});
      database.execute(
          'CREATE TABLE words (word_id INTEGER PRIMARY KEY autoincrement,word TEXT,word_meaning TEXT,group_id INTEGER,in_favorite INTEGER default 0, in_edit INTEGER default 0,foreign key(group_id) REFERENCES groups(group_id))')
          .then((value) {print('words\'Table Created');})
          .catchError((error) {print('Error When Creating words\'s Table ${error.toString()}');});},
        onOpen: (database) async{
           getDataFromGroupTable(database);
           getDataFromWordsTable(database);
           print(('database opened'));})
          .then((value) {
           database = value;
           emit(LanguageAppCreateDatabaseState());
    });
  }
  void printGroupModel(GroupModel model){
    print("groupId=${model.groupId},groupName=${model.groupName},groupCategory=${model.groupCategory}");
  }
  void printWordModel(WordModel model){
    print("wordId=${model.wordId},word=${model.word},wordMeaning=${model.wordMeaning},groupId=${model.groupId},inEdit=${model.inEdit},inFavorite=${model.inFavorite}");
  }
  void insertIntoGroupsTable(
      {required GroupModel groupModel}) async {
          groupModel.groupId=await database?.insert('groups',groupModel.toMap()).then((value) {
          emit(LanguageAppInsertIntoGroupsTableState());
          getDataFromGroupTable(database);
          print("$value inserted successfully");
        }).catchError((error) {
          print("Error When Inserting New Record ${error.toString()}");
        });
  }

  void insertIntoWordsTable({required WordModel wordModel}) async {
          wordModel.wordId= await database?.insert('words', wordModel.toMap()).then((value) {
          emit(LanguageAppInsertIntoWordsTableState());
          getDataFromWordsTable(database);
          print("$value inserted successfully");
        }).catchError((error) {
          print("Error When Inserting New Record ${error.toString()}");
    });
  }

  //List<Map<String, dynamic>> groups = [],words=[],favoriteWords=[];

  void getDataFromGroupTable(database) {
    groups = [];
    emit(LanguageAppGetGroupsTableDataLoadingState());
      database?.rawQuery('SELECT * from groups').then((value) {
      value.forEach((element) {
        groups.add(GroupModel.fromMap(element));
      });
      for(int i=0;i<groups.length;++i){
         printGroupModel(groups[i]!);
      }
      emit(LanguageAppGetGroupsTableDataSuccessState());
    });
  }

  void getDataFromWordsTable(database) {
    words = [];
    emit(LanguageAppGetWordsTableDataLoadingState());
    database?.rawQuery('SELECT * from words').then((value) {
      value.forEach((element) {
        words.add(WordModel.fromMap(element));
      });
      for(int i=0;i<words.length;++i){
         printWordModel(words[i]!);
      }
      emit(LanguageAppGetWordsTableDataSuccessState());
    });
  }

  void deleteFromWordsTable({required id}) {
    emit(LanguageAppDeleteFromWordsTableLoadingState());
    database?.rawDelete('DELETE FROM words WHERE word_id = ?', [id]).then((value) {
      getDataFromWordsTable(database);
      emit(LanguageAppDeleteFromWordsTableSuccessState());
    }).onError((error, stackTrace){
      emit(LanguageAppDeleteFromWordsTableErrorState(error.toString()));
        print(error.toString());
    });
  }

  void updateWordTableData({required id, required  word, required wordMeaning,required inEdit}){
    emit(LanguageAppUpdateWordsTableLoadingState());
    database?.rawUpdate('UPDATE words SET word = ? , word_meaning =? ,in_edit= ?  WHERE word_id = ?',
        ['$word','$wordMeaning','$inEdit', id]).then((value) {
        getDataFromWordsTable(database);
        emit(LanguageAppUpdateWordsTableSuccessState());
    }).onError((error, stackTrace) {
        emit(LanguageAppUpdateWordsTableErrorState(error.toString()));
        print("error occurred when update date to word table "+error.toString());
    });
  }

  void changeFavoriteState(id, inFavorite){
    inFavorite=(inFavorite==1)?0:1;
    database?.rawUpdate('UPDATE words SET in_favorite = ? WHERE word_id = ?',
        ['$inFavorite', id]).then((value) {
      getDataFromWordsTable(database);
      emit(LanguageAppUpdateWordsTableSuccessState());
    }).onError((error, stackTrace) {
      emit(LanguageAppUpdateWordsTableErrorState(error.toString()));
      print("error occurred when update date to word table "+error.toString());
    });
    emit(LanguageAppChangeFavoriteIconState());
  }

  void changeEditState(id,inEdit){
    inEdit=(inEdit==1)?0:1;
    database?.rawUpdate('UPDATE words SET in_edit = ? WHERE word_id = ?',
        ['$inEdit', id]).then((value) {
      getDataFromWordsTable(database);
      emit(LanguageAppUpdateWordsTableSuccessState());
    }).onError((error, stackTrace) {
      emit(LanguageAppUpdateWordsTableErrorState(error.toString()));
      print("error occurred when update date to word table "+error.toString());
    });
    emit(LanguageAppChangeFavoriteIconState());
  }

  void deleteFromGroupsTable({required id}) {
    emit(LanguageAppDeleteFromGroupsTableLoadingState());
    database?.rawDelete('DELETE FROM words WHERE group_id = ?', [id]).then((value) {
      getDataFromWordsTable(database);
      database?.rawDelete('DELETE FROM groups WHERE group_id = ?', [id]).then((value) {
        getDataFromGroupTable(database);
        emit(LanguageAppDeleteFromGroupsTableSuccessState());
      }).onError((error, stackTrace){
        emit(LanguageAppDeleteFromGroupsTableErrorState(error.toString()));
        print(error.toString());
      });
    });
  }
  void updateGroupsTableData({required id, required  groupName, required groupCategory}){
    emit(LanguageAppUpdateGroupsTableLoadingState());
    database?.rawUpdate('UPDATE groups SET group_name = ? , group_category = ?   WHERE group_id = ?',
        ['$groupName','$groupCategory',id]).then((value) {
      getDataFromGroupTable(database);
      emit(LanguageAppUpdateGroupsTableSuccessState());
    }).onError((error, stackTrace) {
      emit(LanguageAppUpdateGroupsTableErrorState(error.toString()));
      print("error occurred when update date to group table "+error.toString());
    });
  }
   int quizPageIndex=0;
   void goToNextQuestion(int index){
    quizPageIndex=index;
    emit(LanguageAppGoToNextQuestionState());
  }
  LoginModel? getUserModel;
  void getUserData() {
    emit(LanguageAppLoadingGetUserDataState());
    DioHelper.getData(
      url: PROFILE,
      token: token,
    )?.then((value) {
      getUserModel = LoginModel.fromJson(value?.data);
      emit(LanguageAppSuccessGetUserDataState());
    }).catchError((error) {

      emit(LanguageAppErrorGetUserState(error.toString()));
    });
  }
  void updateUserData({
    required String name,
    required String phone,
    required String email,
  }) {
    emit(LanguageAppLoadingEditUserDataState());
    DioHelper.putData(
      url: UPDATE_PROFILE,
      token: token,
      data: {
        "name": name,
        "phone": phone,
        "email": email,
      },
    )?.then((value) {
      getUserModel = LoginModel.fromJson(value?.data);
      emit(LanguageAppSuccessEditUserDataState());
    }).catchError((error) {
      emit(LanguageAppAppErrorEditUserState(error.toString()));
    });
  }
}
