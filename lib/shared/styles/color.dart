import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

Color  defaultColor=HexColor('400D5D');
Color  defaultColor1=HexColor('F0D9FF');
Color  defaultColor2=Colors.white;

Color defaultColor3=HexColor('d896ff');
Color defaultColor4=HexColor('be29ec');
Color defaultColor5=HexColor('800080');
Color defaultColor6=HexColor('660066');

List<Color> primariesColor = [
  Colors.red,
  Colors.pink,
  Colors.purple,
  Colors.deepPurple,
  Colors.indigo,
  Colors.blue,
  Colors.lightBlue,
  Colors.cyan,
  Colors.teal,
  Colors.green,
  Colors.lightGreen,
  Colors.lime,
  Colors.yellow,
  Colors.amber,
  Colors.orange,
  Colors.deepOrange,
  Colors.brown,
  Colors.blueGrey,
];
