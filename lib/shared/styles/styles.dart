import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:MemoryApp/shared/styles/color.dart';

ThemeData lightTheme= ThemeData(
    appBarTheme: AppBarTheme(
        color: defaultColor,
        titleSpacing: 40,
        elevation: 0,
        toolbarHeight: 120,
        centerTitle: true,
        iconTheme:IconThemeData(color: defaultColor1),
        /*actionsIconTheme: IconThemeData(color: defaultColor2),*/
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: defaultColor,
            statusBarIconBrightness: Brightness.light),
            titleTextStyle: TextStyle(
            fontSize: 35,
            fontWeight: FontWeight.bold,
            color: defaultColor1,),),
    progressIndicatorTheme: ProgressIndicatorThemeData(
      color: defaultColor
    ),
    primaryColor:defaultColor1,
    inputDecorationTheme:InputDecorationTheme(
      prefixIconColor: defaultColor,
      suffixIconColor:defaultColor,
      labelStyle: TextStyle(color:defaultColor,fontSize: 18),
      enabledBorder: OutlineInputBorder(
          borderSide:BorderSide(color: defaultColor,width: 2.5),
          borderRadius:const BorderRadius.all(Radius.circular(25))),
      focusedBorder: OutlineInputBorder(
          borderSide:BorderSide(color: defaultColor,width: 2.5),
          borderRadius:const BorderRadius.all(Radius.circular(25))),
      /*border: OutlineInputBorder(
          borderSide:BorderSide(color: defaultColor,width: 2.5),
          borderRadius:const BorderRadius.all(Radius.circular(25))),*/
      errorBorder: const OutlineInputBorder(
          borderSide:BorderSide(color: Colors.red,width: 2.5),
          borderRadius:const BorderRadius.all(Radius.circular(25))),
      focusedErrorBorder:const  OutlineInputBorder(
          borderSide:BorderSide(color: Colors.red,width: 2.5),
          borderRadius:const BorderRadius.all(Radius.circular(25))),
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(backgroundColor:defaultColor,foregroundColor: defaultColor1),
    bottomNavigationBarTheme:  BottomNavigationBarThemeData(
      type: BottomNavigationBarType.fixed,
      selectedItemColor: defaultColor1,
      backgroundColor: defaultColor,
      unselectedItemColor: defaultColor5,
    ),
    scaffoldBackgroundColor: defaultColor1,
    backgroundColor: defaultColor,
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: defaultColor,
      selectionColor: defaultColor4,
      selectionHandleColor: defaultColor4
    ),
    textTheme:  TextTheme(
        bodyText1: TextStyle(
            color: defaultColor,
            fontSize: 20,
            fontWeight: FontWeight.bold)),
    iconTheme: IconThemeData(
      color: defaultColor,
      size: 30,
      opacity: 12
    )
    );

ThemeData darkTheme=ThemeData(
    appBarTheme: AppBarTheme(
        titleSpacing: 20,
        color: HexColor('333739'),
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: HexColor('333739'),
            statusBarIconBrightness: Brightness.light),
        titleTextStyle: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.white),
        iconTheme: const IconThemeData(color: Colors.white)),
    progressIndicatorTheme:
    const ProgressIndicatorThemeData(color: Colors.deepOrange),
    inputDecorationTheme: const InputDecorationTheme(
      labelStyle: TextStyle(color: Colors.deepOrange),
      prefixIconColor: Colors.deepOrange,
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.deepOrange),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.deepOrange),
      ),
    ),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
        backgroundColor: Colors.deepOrangeAccent),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      type: BottomNavigationBarType.fixed,
      backgroundColor: HexColor('333739'),
      selectedItemColor: Colors.deepOrangeAccent,
      unselectedItemColor: Colors.grey,
    ),
    textTheme: const TextTheme(
        bodyText1: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w600)),
    scaffoldBackgroundColor: HexColor('333739'),
    backgroundColor: Colors.white);
