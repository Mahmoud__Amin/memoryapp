
const LOGIN='login';

const REGISTER='register';

const HOME='home';

const CATEGORY='categories';

const FAVORITES='favorites';

const PROFILE='profile';

const UPDATE_PROFILE='update-profile';

const PRODUCT_SEARCH='products/search';