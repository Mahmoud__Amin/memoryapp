import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerHelper extends StatelessWidget {
  static File? image;
  static ImagePicker picker =ImagePicker();

  const ImagePickerHelper({Key? key}) : super(key: key);
  static Future  getImage(ImageSource src) async {
    final pickedFile= await picker.pickImage(source: src);

      if(pickedFile!=null){
        image=File(pickedFile.path);
      }
      else{
        print("Image not selected");
      }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: image==null?const Text("No image selected. "):Image.file(image!)
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_a_photo),
        onPressed: (){
          var ad =AlertDialog(
            title: Text("Choose Picture from:"),
            content: Container(
              height: 150,
              child: Column(
                children: [
                  Divider(color: Colors.black,),
                  Container(
                    color: Colors.teal,
                    child: ListTile(
                      leading: Icon(Icons.image),
                      title: Text("Gallery "),
                      onTap:(){
                        getImage(ImageSource.gallery);
                        Navigator.of(context).pop();
                      } ,
                    ),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    color: Colors.teal,
                    child: ListTile(
                      leading: Icon(Icons.add_a_photo),
                      title: Text("Camera"),
                      onTap:(){
                        getImage(ImageSource.camera);
                        Navigator.of(context).pop();
                      } ,
                    ),
                  ),

                ],

              ),
            ),
          );
          showDialog(context: context, builder: (_){
            return ad;
          });
        },
      ),
    );
  }
}
