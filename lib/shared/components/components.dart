import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import '../../models/group_model.dart';
import '../../modules/add_group/add_group_screen.dart';
import '../../modules/quiz_screen/quiz_screen.dart';
import '../../modules/word_screen/word_screen.dart';
import '../cubit/cubit.dart';
import '../styles/color.dart';

/*Widget buildBoardingItem(BuildContext context, PageBoardingModels model) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Image.asset(model.image),
      const SizedBox(height: 100),
      Text(model.title, style: Theme.of(context).textTheme.bodyText1),
      const SizedBox(height: 30),
      Text(model.body),
    ],
  );
}*/

Widget defaultFromField({
  required TextEditingController controller,
  required TextInputType type,
  void Function(String value)? onSubmit,
  void Function(String value)? onChange,
  void Function()? onTap,
  bool isPassword = false,
  required String? Function(String? value) validate,
  required String label,
  IconData? prefix,
  IconData? suffix,
  void Function()? suffixPressed,
  bool isClickable = true,
  TextStyle? style
}) =>
    TextFormField(
      controller: controller,
      keyboardType: type,
      obscureText: isPassword,
      validator: validate,
      onChanged: onChange,
      onTap: onTap,
      readOnly: !isClickable,
      onFieldSubmitted: onSubmit,
      decoration: InputDecoration(
          label: Text(label,style: style),
          prefixIcon: Icon(prefix),
          suffixIcon: IconButton(onPressed: suffixPressed, icon: Icon(suffix)),
          border: const OutlineInputBorder()),
    );

/*
Widget defaultButton({
  double width = double.infinity,
  Color background = Colors.blue,
  required void Function() function,
  required String text,
  TextStyle? style,
  double radius = 0.0,
  bool isUpperCase = true,
}) =>
    Container(
      width: width,
      height: 40.0,
      child: MaterialButton(
        onPressed: function,
        child: Text(
          isUpperCase ? text.toUpperCase() : text,
          style:  TextStyle(
            color: defaultColor1,
          ),
        ),
      ),
      decoration: BoxDecoration(
          color: background, borderRadius: BorderRadius.circular(radius)),
    );
*/

/*
Widget defaultTextButton({
  required void Function() function,
  required String text,
}) =>
    TextButton(onPressed: function, child: Text(text.toUpperCase(), ));
*/

void navigateTo(BuildContext context, Widget widget) {
  Navigator.push(
      context, MaterialPageRoute(builder: (BuildContext context) => widget));
}

void navigateReplace(context, Widget widget) {
  Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (BuildContext context) => widget));
}

void defaultToast({
  required String message,
  required ToastStates state,
}) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: choseToastColor(state),
      textColor: Colors.white,
      fontSize: 16.0);
}

enum ToastStates { SUCCESS, ERROR, WARNING }

Color choseToastColor(ToastStates toastStates) {
  if (toastStates == ToastStates.SUCCESS)
    return defaultColor;
  else if (toastStates == ToastStates.ERROR) defaultColor;
  return defaultColor;
}

List<String> images = [
  "assets/images/Words.jpg",
  "assets/images/Numbers.jpg",
  "assets/images/Information.jpg",
  "assets/images/Others.jpg",
  "assets/images/Words1.jpg",
  "assets/images/Numbers1.jpg",
  "assets/images/Information1.jpg",
  "assets/images/Others1.jpg",
];

String selectImage(String value){
   if(value=="Words")return images[0];
   else if(value=="Numbers")return images[1];
   else if(value=="Information")return images[2];
   else  return images[3];
}
String selectImage1(String value){
  if(value=="Words")return images[4];
  else if(value=="Numbers")return images[5];
  else if(value=="Information")return images[6];
  else  return images[7];
}
Widget divider() => Padding(
      padding: const EdgeInsetsDirectional.only(
        start: 20.0,
      ),
      child: Container(
        width: double.infinity,
        height: 1.0,
        color: Colors.black,
      ),
    );
Widget divider1()=>Padding(
  padding: const EdgeInsetsDirectional.only(
    start: 0,
  ),
  child: Container(
    width: 1.0,
    height: 200,
    color: Colors.black,
  ),
);
Widget buildGridView(context,bool toQuiz){
  var cubit =LanguageAppCubit.get(context);
  return GridView.count(
      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 15),
      crossAxisCount: 2,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      //childAspectRatio: 1.1,
      mainAxisSpacing: 20,
      crossAxisSpacing: 20,
      children: List.generate(cubit.groups.length,(index) => buildGroupItem(cubit.groups[index]!,context,toQuiz))

  );
}
Widget buildGroupItem(GroupModel group,context,bool toQuiz){
  return InkWell(
    onTap: (){
      (!toQuiz)?navigateTo(context,WordScreen(group:group)):navigateTo(context,QuizScreen(group:group));
    },
    onLongPress: (){
      showEditAndDeleteGroupDialog(context,group);
    },
    child: SizedBox(
      width: double.infinity,
      height: 160,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(selectImage(group.groupCategory!)),
                  fit: BoxFit.fill,
                  filterQuality: FilterQuality.high,
                ),
                color: defaultColor2,
                borderRadius: BorderRadius.circular(25),
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(4, 4),
                    color: defaultColor,
                    blurRadius: 5,
                    //spreadRadius: 5
                  ),
                ],
                border: Border.all(
                  color: defaultColor,
                  width: 3,
                )),
          ),
          Align(
              alignment: const Alignment(0.7, 0.8),
              child: Text(
                group.groupName!,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.copyWith(fontSize: 15),
                overflow: TextOverflow.ellipsis,
              )),
        ],
      ),
    ),
  );
}

 BoxDecoration defaultBoxDecoration(){
   return  BoxDecoration(
       color: defaultColor2,
       borderRadius: BorderRadius.circular(25),
       boxShadow: [
         BoxShadow(
           offset: Offset(4, 4),
           color: defaultColor,
           blurRadius: 5,
           //spreadRadius: 5
         ),
       ],
       border: Border.all(
         color: defaultColor,
         width: 3,
       ));
 }



