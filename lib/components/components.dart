import 'package:flutter/material.dart';
import 'package:MemoryApp/shared/styles/color.dart';

/*Widget buildBoardingItem(BuildContext context, PageBoardingModels model) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Image.asset(model.image),
      const SizedBox(height: 100),
      Text(model.title, style: Theme.of(context).textTheme.bodyText1),
      const SizedBox(height: 30),
      Text(model.body),
    ],
  );
}*/

/*
Widget defaultFromField({
  required TextEditingController controller,
  required TextInputType type,
  void Function(String value)? onSubmit,
  void Function(String value)? onChange,
  void Function()? onTap,
  bool isPassword = false,
  required String? Function(String? value) validate,
  required String label,
  IconData? prefix,
  IconData? suffix,
  void Function()? suffixPressed,
  bool isClickable = true,
  TextStyle? style,
}) =>
    TextFormField(
      controller: controller,
      keyboardType: type,
      obscureText: isPassword,
      validator: validate,
      onChanged: onChange,
      onTap: onTap,
      onFieldSubmitted: onSubmit,
      decoration: InputDecoration(
          label: Text(label,style: style,),
          prefixIcon: Icon(prefix),
          suffixIcon: IconButton(onPressed: suffixPressed, icon: Icon(suffix))),
    );
*/

Widget defaultButton({
  double width = double.infinity,
  Color background = Colors.blue,
  required void Function() function,
  required String text,
  TextStyle? style,
  double radius = 0.0,
  bool isUpperCase = true,
}) =>
    Container(
      width: width,
      height: 40.0,
      child: MaterialButton(
        onPressed: function,
        child: Text(
          isUpperCase ? text.toUpperCase() : text,
          style:  TextStyle(
            color: defaultColor1,
          ),
        ),
      ),
      decoration: BoxDecoration(
          color: background, borderRadius: BorderRadius.circular(radius)),
    );
Widget defaultTextButton({
  required void Function() function,
  required String text,
  Color color=Colors.white,
  bool toUpperCase=true,
}) =>
    TextButton(
        onPressed: function,
        child: Text(
          (toUpperCase)?text.toUpperCase():text,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: color,
          ),
        ));

/*void navigateTo(BuildContext context, Widget widget) {
  Navigator.push(
      context, MaterialPageRoute(builder: (BuildContext context) => widget));
}*/

/*void navigateReplace(BuildContext context, Widget widget) {
  Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (BuildContext context) => widget));
}*/

/*
void defaultToast({
  required String message,
  required ToastStates state,
}) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: choseToastColor(state),
      textColor: Colors.white,
      fontSize: 16.0);

}*/

/*enum ToastStates { SUCCESS, ERROR, WARNING }*/

/*Color choseToastColor(ToastStates toastStates) {
  if (toastStates == ToastStates.SUCCESS)
    return Colors.green;
  else if (toastStates == ToastStates.ERROR) return Colors.red;
  return Colors.amber;
}*/
