
import 'package:flutter/material.dart';
class GroupModel{
  int? groupId;
  String? groupName;
  String? groupCategory;

    Map<String,dynamic> toMap(){
      Map<String,dynamic> map={};
      map['group_name']=groupName;
      map['group_category']=groupCategory;
      if(groupId!=null)map['group_id']=groupId;
      return map;
   }

    GroupModel({groupId,required groupName,required groupCategory}){
      if(groupId!=null)this.groupId=groupId;
      this.groupName=groupName;
      this.groupCategory=groupCategory;
   }

    GroupModel.fromMap(Map<String,dynamic> map){
    groupId=map['group_id'];
    groupName=map['group_name'];
    groupCategory=map['group_category'];
  }

}
