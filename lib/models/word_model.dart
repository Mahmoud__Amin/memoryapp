class WordModel {
  int? wordId;
  String? word;
  String? wordMeaning;
  int? groupId;
  int? inFavorite;
  int? inEdit;

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {};
    if(wordId!=null)map['word_id'] = wordId;
    map['word'] = word;
    map['word_meaning'] = wordMeaning;
    map['group_id'] = groupId;
    map['in_favorite'] = inFavorite;
    map['in_edit'] = inEdit;
    return map;
  }
  
  WordModel ({required word,required wordMeaning,required groupId,inFavorite=0,inEdit=0}){
       this.word=word;
       this.wordMeaning=wordMeaning;
       this.groupId=groupId;
       this.inFavorite=inFavorite;
       this.inEdit=inEdit;
  }

  WordModel.fromMap(Map<String, dynamic> map) {
    wordId = map['word_id'];
    word = map['word'];
    wordMeaning = map['word_meaning'];
    groupId = map['group_id'];
    inFavorite = map['in_favorite'];
    inEdit = map['in_edit'];
  }
}
