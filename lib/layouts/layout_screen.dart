import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:MemoryApp/shared/image_picker_helper.dart';
import '../components/components.dart';
import '../shared/components/constans.dart';
import '../shared/cubit/cubit.dart';
import '../shared/cubit/states.dart';
import '../shared/styles/color.dart';


class LayoutScreen extends StatelessWidget {
  var scaffoldKey = GlobalKey<ScaffoldState>();

  var formKey = GlobalKey<FormState>();

  var wordController = TextEditingController();

  var wordDefinitionController = TextEditingController();

  var wordImageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var cubit=LanguageAppCubit.get(context);
    return BlocConsumer<LanguageAppCubit,LanguageAppStates>(
      listener: (BuildContext context, state) {  },
      builder: (BuildContext context, state)=>Scaffold(
        key: scaffoldKey,
        appBar: AppBar(titleSpacing:20,centerTitle:false,toolbarHeight:56,title: const Text("Memory App"),actions: [
           /*Padding(
             padding: const EdgeInsets.all(8.0),
             child: defaultTextButton(function:(){logOut(context);}, text: "Logout",toUpperCase: false,color: defaultColor4),
           )*/
        ],),
        body: cubit.bodyList[cubit.currentIndex],
        bottomNavigationBar: BottomNavigationBar(
            currentIndex: cubit.currentIndex,
            onTap: (index){
              cubit.changeBottomNavigationBarIndex(index);
            },
            items:cubit.items
        ),
      ),
    );
  }
  void choosePictureDialog(context){
    var ad =AlertDialog(
      title: Text("Choose Picture from:"),
      content: Container(
        height: 150,
        child: Column(
          children: [
            Divider(color: Colors.black,),
            Container(
              color: Colors.teal,
              child: ListTile(
                leading: Icon(Icons.image),
                title: Text("Gallery "),
                onTap:(){
                  ImagePickerHelper.getImage(ImageSource.gallery);
                  Navigator.of(context).pop();
                } ,
              ),
            ),
            SizedBox(height: 10,),
            Container(
              color: Colors.teal,
              child: ListTile(
                leading: Icon(Icons.add_a_photo),
                title: Text("Camera"),
                onTap:(){
                  ImagePickerHelper.getImage(ImageSource.camera);
                  Navigator.of(context).pop();
                } ,
              ),
            ),

          ],

        ),
      ),
    );
    showDialog(context: context, builder: (_){
      return ad;
    });

  }
}


